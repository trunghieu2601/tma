package org.arpit.java2blog.controller;

import org.arpit.java2blog.model.Account;
import org.arpit.java2blog.model.Device;
import org.arpit.java2blog.service.AccountService;
import org.arpit.java2blog.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public class AccountController {
    @Autowired
    private AccountService accountService;

    @PostMapping("/save")
    public Account save(@RequestBody Account account){
        System.out.println("okkkkkkkkkk");
        return accountService.save(account);
    }
    @GetMapping("/all")
    public List<Account> getAllAccount(){
        List<Account> list = accountService.getAllAccount();
        return list;
    }
    @GetMapping("/allcriteria")
    public List<Account> getAllAccountCriteria(){
        List<Account> list = accountService.getAllAccountCriteria();
        return list;
    }

    @GetMapping("/{id}")
    public Account getAccountId(@PathVariable(value="id") Long accountId){
        return accountService.getAccountId(accountId);
    }

    @PutMapping("/update/{id}")
    public void updateAccountId(@RequestBody Account updateAccount,@PathVariable(value="id") Long accountId){
        accountService.updateAccountId(updateAccount,accountId);
    }
    @DeleteMapping("/delete/{id}")
    public void deleteAccountId(@PathVariable (value="id") Long accountId){
        accountService.deleteAccountId(accountId);
    }
}
