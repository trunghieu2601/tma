
package org.arpit.java2blog.controller;
import org.arpit.java2blog.model.Device;
import org.arpit.java2blog.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tma/device")
public class DeviceController {


    @Autowired
    private DeviceService deviceService;

    @PostMapping("/save")
    public Device save(@RequestBody Device device){
        System.out.println("okkkkkkkkkk");
        return deviceService.save(device);
    }
    @GetMapping("/all")
    public List<Device> getAllDevice(){
        List<Device> list = deviceService.getAllDevice();
        return list;
    }
    @GetMapping("/allcriteria")
    public List<Device> getAllDeviceCriteria(){
        List<Device> list = deviceService.getAllDeviceCriteria();
        return list;
    }

    @GetMapping("/{id}")
    public Device getDeviceId(@PathVariable(value="id") Long deviceId){
        return deviceService.getDeviceId(deviceId);
    }

    @PutMapping("/update/{id}")
    public void updateDeviceId(@RequestBody Device updateDevice,@PathVariable(value="id") Long deviceId){
        deviceService.updateDeviceId(updateDevice,deviceId);
    }
    @DeleteMapping("/delete/{id}")
    public void deleteDeviceId(@PathVariable (value="id") Long deviceId){
        deviceService.deleteDeviceId(deviceId);
    }
}
