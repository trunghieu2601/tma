package org.arpit.java2blog.controller;
import org.arpit.java2blog.model.TypeDevice;
import org.arpit.java2blog.service.TypeDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/tma/type-device")
public class TypeDeviceController {
    @Autowired
    private TypeDeviceService typeDeviceService;

    @PostMapping("/save")
    public TypeDevice save(@RequestBody TypeDevice typeDevice){
        return typeDeviceService.save(typeDevice);
    }

    @GetMapping("/all")
    public List<TypeDevice> getAllTypeDevice(){
        List<TypeDevice> list = typeDeviceService.getAllTypeDevice();

        return list;
    }

    @GetMapping("/allcriteria")
    public List<TypeDevice> getAllTypeDeviceCriteria(){
        List<TypeDevice> list = typeDeviceService.getAllTypeDeviceCriteria();
        return list;
    }

    @GetMapping("/{id}")
    public TypeDevice getTypeDeviceId(@PathVariable(value="id") Long typeDeviceId){
        return typeDeviceService.getTypeDeviceId(typeDeviceId);
    }

    @PutMapping("/update/{id}")
    public void updateTypeDeviceId(@RequestBody TypeDevice updateTypeDevice,@PathVariable(value="id") Long typeDeviceId){
            typeDeviceService.updateTypeDeviceId(updateTypeDevice,typeDeviceId);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteTypeDeviceId(@PathVariable (value="id") Long typeDeviceId){
            typeDeviceService.deleteTypeDeviceId(typeDeviceId);
    }
}
