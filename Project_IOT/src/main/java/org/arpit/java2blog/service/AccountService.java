package org.arpit.java2blog.service;

import org.arpit.java2blog.model.Account;
import org.arpit.java2blog.reponsitory.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AccountService {
    @Autowired
    private AccountRepository accountRepository;

    public List<Account> getAllAccount(){
        return accountRepository.getAllAccount();
    }
    public List<Account> getAllAccountCriteria(){
        return accountRepository.getAllAccountCriteria();
    }
    public Account save(Account account){
        return accountRepository.save(account);
    }

    public Account getAccountId(Long accountId){
        return accountRepository.getAccountId(accountId);
    }

    public void updateAccountId(Account updateAccount,Long accountId){
        accountRepository.updateAccountId(updateAccount,accountId);
    }
    public void deleteAccountId(Long accountId){
        accountRepository.deleteAccountId(accountId);
    }
}
