package org.arpit.java2blog.service;

import org.arpit.java2blog.model.TypeDevice;
import org.arpit.java2blog.reponsitory.TypeDeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeDeviceService {

    @Autowired
    private TypeDeviceRepository typeDeviceRepository;
    public List<TypeDevice> getAllTypeDevice(){
        return typeDeviceRepository.getAllTypeDevice();
    }
    public List<TypeDevice> getAllTypeDeviceCriteria(){
        return typeDeviceRepository.getAllTypeDeviceCriteria();
    }
    public TypeDevice save(TypeDevice typeDevice){
        return typeDeviceRepository.save(typeDevice);
    }

    public TypeDevice getTypeDeviceId(Long typeDeviceId){
        return typeDeviceRepository.getTypeDeviceId(typeDeviceId);
    }

    public void updateTypeDeviceId(TypeDevice updateTypeDevice,Long typeDeviceId){
            typeDeviceRepository.updateTypeDeviceId(updateTypeDevice,typeDeviceId);
    }
    public void deleteTypeDeviceId(Long typeDeviceId){
        typeDeviceRepository.deleteTypeDeviceId(typeDeviceId);
    }
}
