package org.arpit.java2blog.service;

import org.arpit.java2blog.model.Device;
import org.arpit.java2blog.reponsitory.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeviceService {
    @Autowired
    private DeviceRepository deviceRepository;

    public List<Device> getAllDevice(){
        return deviceRepository.getAllDevice();
    }
    public List<Device> getAllDeviceCriteria(){
        return deviceRepository.getAllDeviceCriteria();
    }
    public Device save(Device device){
        return deviceRepository.save(device);
    }

    public Device getDeviceId(Long deviceId){
        return deviceRepository.getDeviceId(deviceId);
    }

    public void updateDeviceId(Device updateDevice,Long deviceId){
        deviceRepository.updateDeviceId(updateDevice,deviceId);
    }
    public void deleteDeviceId(Long deviceId){
        deviceRepository.deleteDeviceId(deviceId);
    }
}
