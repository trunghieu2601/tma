package org.arpit.java2blog.reponsitory;

import org.arpit.java2blog.model.TypeDevice;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class TypeDeviceRepository {
    @Autowired
    private SessionFactory sessionFactory;

    public TypeDevice save(TypeDevice typeDevice){
        Session session=sessionFactory.getCurrentSession();
        session.save(typeDevice);
        return typeDevice;
    }
    public List<TypeDevice> getAllTypeDevice() {
            Session session=sessionFactory.getCurrentSession();
            List <TypeDevice> list=session.createQuery("from TypeDevice").list();
            return list;
    }
    public List<TypeDevice> getAllTypeDeviceCriteria() {
        Session session=sessionFactory.getCurrentSession();
        Criteria criteria =session.createCriteria(TypeDevice.class);
        return criteria.list();
    }

    public TypeDevice getTypeDeviceId(Long typeDeviceId){
        Session session=sessionFactory.getCurrentSession();
        TypeDevice typeDevice=(TypeDevice)session.get(TypeDevice.class,typeDeviceId);
        return typeDevice;
    }

    public void updateTypeDeviceId(TypeDevice updateTypeDevice,Long typeDeviceId){
        Session session=sessionFactory.getCurrentSession();
        TypeDevice typeDevice=(TypeDevice) session.get(TypeDevice.class,typeDeviceId);
        if(typeDevice!=null){
            typeDevice.setName(updateTypeDevice.getName());
            typeDevice.setDescription(updateTypeDevice.getDescription());
            session.update(typeDevice);
        }
    }
    public void deleteTypeDeviceId(Long typeDeviceId){
        Session session=sessionFactory.getCurrentSession();
        TypeDevice typeDevice=(TypeDevice) session.load(TypeDevice.class,typeDeviceId);
        if(typeDevice!=null){
            session.delete(typeDevice);
        }
    }
}
