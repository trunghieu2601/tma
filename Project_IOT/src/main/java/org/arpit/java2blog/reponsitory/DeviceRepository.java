package org.arpit.java2blog.reponsitory;

import org.arpit.java2blog.model.Device;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DeviceRepository {
    @Autowired
    private SessionFactory sessionFactory;

    public Device save(Device device){
        Session session=sessionFactory.getCurrentSession();
        session.save(device);
        return device;
    }
    public List<Device> getAllDevice() {
        Session session=sessionFactory.getCurrentSession();
        List <Device> list=session.createQuery("from Device").list();
        return list;
    }
    public List<Device> getAllDeviceCriteria() {
        Session session=sessionFactory.getCurrentSession();
        Criteria criteria =session.createCriteria(Device.class);
        return criteria.list();
    }

    public Device getDeviceId(Long deviceId){
        Session session=sessionFactory.getCurrentSession();
        Device device=(Device)session.get(Device.class,deviceId);
        return device;
    }

    public void updateDeviceId(Device updateDevice,Long deviceId){
        Session session=sessionFactory.getCurrentSession();
        Device device=(Device) session.get(Device.class,deviceId);
        if(device!=null){
            device.setName(updateDevice.getName());
            device.setDescription(updateDevice.getDescription());
            session.update(device);
        }
    }
    public void deleteDeviceId(Long deviceId){
        Session session=sessionFactory.getCurrentSession();
        Device device=(Device) session.load(Device.class,deviceId);
        if(device!=null){
            session.delete(device);
        }
    }
}
