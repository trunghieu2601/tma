package org.arpit.java2blog.reponsitory;

import org.arpit.java2blog.model.Account;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AccountRepository {
    @Autowired
    private SessionFactory sessionFactory;

    public Account save(Account account){
        Session session=sessionFactory.getCurrentSession();
        session.save(account);
        return account;
    }
    public List<Account> getAllAccount() {
        Session session=sessionFactory.getCurrentSession();
        List <Account> list=session.createQuery("from Account").list();
        return list;
    }
    public List<Account> getAllAccountCriteria() {
        Session session=sessionFactory.getCurrentSession();
        Criteria criteria =session.createCriteria(Account.class);
        return criteria.list();
    }

    public Account getAccountId(Long accountId){
        Session session=sessionFactory.getCurrentSession();
        Account account=(Account)session.get(Account.class,accountId);
        return account;
    }

    public void deleteAccountId(Long accountId){
        Session session=sessionFactory.getCurrentSession();
        Account account=(Account) session.load(Account.class,accountId);
        if(account!=null){
            session.delete(account);
        }
    }
}
