package org.arpit.java2blog.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="Device")
public class Device implements Serializable {
   @Id
   @Column(name="deviceId")
   @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long deviceId;

   public void setDeviceId(Long deviceId){
       this.deviceId=deviceId;
   }
   public Long getDeviceId(){
       return deviceId;
   }
   @Column(name="name")
    private String name;
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    @ManyToOne
    @JoinColumn(name = "typeDeviceId")
    private TypeDevice typeDevice;
    public void setTypeDevice(TypeDevice typeDevice){
        this.typeDevice=typeDevice;
    }
    public TypeDevice getTypeDevice(){
        return typeDevice;
    }

    @ManyToOne
    @JoinColumn(name = "userId")
    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Column(name="nameDevice ")
    private String nameDevice;
    public void setNameDevice(String nameDevice ){
        this.nameDevice=nameDevice;
    }
    public String getNameDevide(){
        return nameDevice;
    }
    @Column(name="description")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
