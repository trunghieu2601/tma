package org.arpit.java2blog.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
/*
 * This is our model class and it corresponds to Customer table in database
 */
@Entity
@Table(name="TypeDevice")
public
class TypeDevice implements Serializable {
    @Id
    @Column(name="typedeviceid")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long typedeviceid;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    public TypeDevice() {
        super();
    }


    public Long getTypeDeviceId() {
        return typedeviceid;
    }
    public void setTypeDeviceId(Long typedeviceid) {
        this.typedeviceid = typedeviceid;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        if(createdAt==null){
            Date date = new Date();
            createdAt=date;
        }
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        if(updatedAt==null){
            Date date = new Date();
            updatedAt=date;
        }
        this.updatedAt = updatedAt;
    }
}
